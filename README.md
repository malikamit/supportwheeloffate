## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `/WheelofFate/src/main/java/com/support/wheel/WheelofFateApplication.java` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```
Local Deployment [Click here](http://localhost:8080/)

AWS Deployment [Click here](http://wheeloffate-env.crzbqa7knv.ap-south-1.elasticbeanstalk.com:8080/)

Consumable API endpoint is "{host}/schedule"
For Eg: AWS Rest API is [Click here](http://wheeloffate-env.crzbqa7knv.ap-south-1.elasticbeanstalk.com:8080/schedule)


## Application Details
Design and build an online �Support Wheel of Fate�. This should repeat selecting two
engineers at random to both complete a half day of support (shift) each to ultimately generate a
schedule that shows whose turn is it to support the business.

## Assumptions
Company X has 10 engineers.

The schedule will span two weeks and start on the first working day of the upcoming week.

Assumption that the schedule will span two weeks and start on the first working day of the upcoming week. 
So taking upcoming Monday as next working day.

## General Scenario
1. Application will always generate schedule starting from upcoming monday.
2. Today is Monday then schedule generated starting from today.
3. Clicking "Generate Schedule" generates new random schedule if clicked again.

## Rules
1. An engineer can do at most one half day shift in a day.
2. An engineer cannot have half day shifts on consecutive days.
3. Each engineer should have completed one whole day of support in any 2 week period.

