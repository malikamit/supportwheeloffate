package com.support.wheel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author Amit Malik
 *
 */
@SpringBootApplication
public class WheelofFateApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(WheelofFateApplication.class, args);
	}
}
