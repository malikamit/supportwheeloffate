package com.support.wheel.constant;

/**
 * @author Amit Malik
 *
 */
public enum SHIFT_TYPE {
	QUARTER(4), HALFDAY(2), FULLDAY(1);

	private SHIFT_TYPE(int totalShifts) {
		this.totalShifts = totalShifts;
	}

	private int totalShifts;

	public static SHIFT_TYPE getDefaultShift() {
		return QUARTER;
	}

	public int getTotalShiftsInDay() {
		return this.totalShifts;
	}
}
