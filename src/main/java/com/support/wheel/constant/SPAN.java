package com.support.wheel.constant;

/**
 * @author Amit Malik
 *
 */
public enum SPAN {

	WEEKLY(7), BIWEEKLY(14);

	private int daysCount;

	private SPAN(int daysCount) {
		this.daysCount = daysCount;
	}

	public int getDaysCount() {
		return daysCount;
	}

}
