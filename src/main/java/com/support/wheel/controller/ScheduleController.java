package com.support.wheel.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.support.wheel.exception.ScheduleFailedException;
import com.support.wheel.response.model.ScheduleResponse;
import com.support.wheel.schedule.service.ScheduleService;

/**
 * @author Amit Malik
 *
 */
@RestController
public class ScheduleController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	private ScheduleService scheduleService;

	@RequestMapping(path = "/schedule", method = RequestMethod.GET)
	public String getSchedule() throws ScheduleFailedException {
		StopWatch watch = new StopWatch();
		watch.start();
		logger.info("Schedule generation started.");
		List<ScheduleResponse> schedules = scheduleService.generateSchedule();
		logger.info("Schedule generation finished.");
		watch.stop();
		logger.info("Time taken to generate schedule : " + watch.getTotalTimeMillis() + "ms");
		Gson gson = new Gson();
		return gson.toJson(schedules);
	}

}
