package com.support.wheel.data.cache;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.support.wheel.dao.Engineer;
import com.support.wheel.repository.EngineerRepository;
import com.support.wheel.util.CommonUtils;

/**
 * @author Amit Malik
 *
 */
@Component
public class DataCache {

	@Autowired
	private EngineerRepository engineerRepository;

	private List<Engineer> engineers;

	@PostConstruct
	private void initCache() {
		engineers = engineerRepository.findAll();
	}

	public int getAvailableEngineersCount() {
		return engineers.size();
	}

	public Engineer getRandomEngineer() {
		int randomindex = CommonUtils.getRandomNumber(engineers.size());
		return engineers.get(randomindex);
	}
}
