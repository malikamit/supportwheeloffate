package com.support.wheel.exception;

/**
 * @author Amit Malik
 *
 */
public class ScheduleFailedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ScheduleFailedException(String message) {
		super(message);
	}

}
