package com.support.wheel.exception;

/**
 * @author Amit Malik
 *
 */
public class ScheduleInvalidException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ScheduleInvalidException(String message) {
		super(message);
	}

}
