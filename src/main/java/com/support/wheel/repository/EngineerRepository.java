package com.support.wheel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.support.wheel.dao.Engineer;

/**
 * @author Amit Malik
 *
 */
public interface EngineerRepository extends JpaRepository<Engineer, Integer> {

}
