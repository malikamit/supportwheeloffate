package com.support.wheel.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.support.wheel.dao.Schedule;

/**
 * @author Amit Malik
 *
 */
public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {
	
	public Schedule findByDate(Date date);

}
