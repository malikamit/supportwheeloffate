package com.support.wheel.response.model;

/**
 * @author Amit Malik
 * Model class to represent an engineer
 *
 */
public class Engineer {

	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
