package com.support.wheel.response.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import com.support.wheel.dao.Engineer;
import com.support.wheel.dao.Schedule;

/**
 * @author Amit Malik
 *
 */
public class ScheduleContext {

	private Set<Engineer> invalidEngineers;

	private Engineer engineer;

	private List<Engineer> currentShiftEngineers;

	private List<Schedule> currentSchedules;

	private LinkedHashMap<Engineer, Integer> engineerWeeklyScheduleCountMap;

	public ScheduleContext(Set<Engineer> invalidEngineers, List<Engineer> currentShiftEngineers,
			List<Schedule> currentSchedules, LinkedHashMap<Engineer, Integer> engineerWeeklyScheduleCountMap) {
		super();
		this.invalidEngineers = invalidEngineers;
		this.currentShiftEngineers = currentShiftEngineers;
		this.currentSchedules = currentSchedules;
		this.engineerWeeklyScheduleCountMap = engineerWeeklyScheduleCountMap;
	}

	public LinkedHashMap<Engineer, Integer> getEngineerWeeklyScheduleCountMap() {
		return engineerWeeklyScheduleCountMap;
	}

	public Set<Engineer> getInvalidEngineers() {
		return invalidEngineers;
	}

	public Engineer getEngineer() {
		return engineer;
	}

	public List<Engineer> getCurrentShiftEngineers() {
		return currentShiftEngineers;
	}

	public List<Schedule> getCurrentSchedules() {
		return currentSchedules;
	}

	public void setEngineer(Engineer engineer) {
		this.engineer = engineer;
	}

}
