package com.support.wheel.response.model;

import java.util.Date;

/**
 * @author Amit Malik
 *
 */
public class ScheduleResponse {

	private Date date;
	private String firstHalfEmployeeName;
	private String secondHalfEmployeeName;

	public ScheduleResponse(Date date, String firstHalfEmployeeName, String secondHalfEmployeeName) {
		super();
		this.date = date;
		this.firstHalfEmployeeName = firstHalfEmployeeName;
		this.secondHalfEmployeeName = secondHalfEmployeeName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getFirstHalfEmployeeName() {
		return firstHalfEmployeeName;
	}

	public void setFirstHalfEmployeeName(String firstHalfEmployeeName) {
		this.firstHalfEmployeeName = firstHalfEmployeeName;
	}

	public String getSecondHalfEmployeeName() {
		return secondHalfEmployeeName;
	}

	public void setSecondHalfEmployeeName(String secondHalfEmployeeName) {
		this.secondHalfEmployeeName = secondHalfEmployeeName;
	}

}
