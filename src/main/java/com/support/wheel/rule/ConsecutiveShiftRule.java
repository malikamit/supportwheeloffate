package com.support.wheel.rule;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.support.wheel.dao.Engineer;
import com.support.wheel.dao.Schedule;
import com.support.wheel.response.model.ScheduleContext;

/**
 * @author Amit Malik
 *
 * Rule: An engineer cannot have half day shifts on consecutive days
 */
@Component("ConsecutiveShiftRule")
public class ConsecutiveShiftRule extends Rule {

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Override
	public boolean evaluateRule(ScheduleContext context) {
		logger.info("Evaluating consecutive shift rule");
		return checkForConsecutiveShifts(context.getInvalidEngineers(), context.getCurrentSchedules(),
				context.getEngineer());
	}

	/**
	 * 
	 * @param currentSchedules
	 * @param invalidEngineers
	 * @param engineer
	 */
	private boolean checkForConsecutiveShifts(Set<Engineer> invalidEngineers, List<Schedule> currentSchedules,
			Engineer engineer) {
		if (currentSchedules.size() > 0
				&& currentSchedules.get(currentSchedules.size() - 1).getEngineers().contains(engineer)) {
			return false;
		}
		return true;
	}

}