package com.support.wheel.rule;

import java.util.LinkedHashMap;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.support.wheel.constant.SHIFT_TYPE;
import com.support.wheel.dao.Engineer;
import com.support.wheel.response.model.ScheduleContext;

/**
 * @author Amit Malik
 * 
 * Rule: Each engineer should have completed one whole day of support in
 * any 2 week period
 */
@Component("OneDayinTwoWeeksRule")
public class OneDayinTwoWeeksRule extends Rule {

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Override
	public boolean evaluateRule(ScheduleContext context) {
		logger.info("Evaluating one day in 2 weeks rule");
		return checkForOneDayIn2Weeks(context.getInvalidEngineers(), context.getEngineer(),
				context.getEngineerWeeklyScheduleCountMap());
	}

	/**
	 * @param invalidEngineers
	 * @param engineer
	 * @return
	 */
	private boolean checkForOneDayIn2Weeks(Set<Engineer> invalidEngineers, Engineer engineer,
			LinkedHashMap<Engineer, Integer> engineerWeeklyScheduleCountMap) {
		if (engineerWeeklyScheduleCountMap.get(engineer) != null
				&& engineerWeeklyScheduleCountMap.get(engineer) == SHIFT_TYPE.HALFDAY.getTotalShiftsInDay()) {
			return false;
		}

		return true;
	}

}
