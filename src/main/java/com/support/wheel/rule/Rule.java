package com.support.wheel.rule;

import com.support.wheel.response.model.ScheduleContext;

/**
 * @author Amit Malik
 *
 */
public abstract class Rule {

	private Rule nextRule;

	public void setNextRule(Rule nextRule) {
		this.nextRule = nextRule;
	}

	public boolean processRules(ScheduleContext context) {
		boolean checkPassed = evaluateRule(context);
		if (checkPassed && null != nextRule) {
			checkPassed = nextRule.processRules(context);
			if (!checkPassed) {
				return checkPassed;
			}
		}
		return checkPassed;
	}

	public abstract boolean evaluateRule(ScheduleContext context);
}
