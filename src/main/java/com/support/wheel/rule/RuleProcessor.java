package com.support.wheel.rule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.support.wheel.response.model.ScheduleContext;

/**
 * @author Amit Malik
 *
 */
@Component
public class RuleProcessor {

	@Autowired
	@Qualifier("ConsecutiveShiftRule")
	private Rule consecutiveShiftRule;

	@Autowired
	@Qualifier("OneDayinTwoWeeksRule")
	private Rule oneDayinTwoWeeksRule;

	@Autowired
	@Qualifier("ShiftsInADayRule")
	private Rule shiftsInADayRule;

	public boolean validateEngineer(ScheduleContext context) {
		oneDayinTwoWeeksRule.setNextRule(shiftsInADayRule);
		shiftsInADayRule.setNextRule(consecutiveShiftRule);
		/*
		 * Create more rules and add here to use them.
		 * Order can be changes easily.
		 * 
		 * */
		return oneDayinTwoWeeksRule.processRules(context);
	}

}
