package com.support.wheel.rule;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.support.wheel.dao.Engineer;
import com.support.wheel.response.model.ScheduleContext;

/**
 * @author Amit Malik
 * 
 * Rule: An engineer can do at most one half day shift in a day.
 * 
 */
@Component("ShiftsInADayRule")
public class ShiftsInADayRule extends Rule {

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Override
	public boolean evaluateRule(ScheduleContext context) {
		logger.info("Evaluating one shift in a day rule");
		return checkForAtMostOneHalfShiftInDay(context.getInvalidEngineers(), context.getCurrentShiftEngineers(),
				context.getEngineer());
	}

	/**
	 * @param invalidEngineers
	 * @param currentShiftEngineers
	 * @param engineer
	 */
	private boolean checkForAtMostOneHalfShiftInDay(Set<Engineer> invalidEngineers,
			List<Engineer> currentShiftEngineers, Engineer engineer) {
		if (currentShiftEngineers.contains(engineer)) {
			return false;
		}
		return true;
	}

}