package com.support.wheel.schedule.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.support.wheel.constant.SHIFT_TYPE;
import com.support.wheel.constant.SPAN;
import com.support.wheel.dao.Engineer;
import com.support.wheel.dao.Schedule;
import com.support.wheel.data.cache.DataCache;
import com.support.wheel.exception.ScheduleFailedException;
import com.support.wheel.exception.ScheduleInvalidException;
import com.support.wheel.response.model.ScheduleContext;
import com.support.wheel.response.model.ScheduleResponse;
import com.support.wheel.rule.RuleProcessor;
import com.support.wheel.util.CommonUtils;
import com.support.wheel.util.DtoUtils;

/**
 * @author Amit Malik
 *
 */
@Service
public class ScheduleService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	private DataCache dataCache;

	@Autowired
	private RuleProcessor ruleProcessor;

	private LinkedHashMap<Engineer, Integer> engineerWeeklyScheduleCountMap = new LinkedHashMap<>();

	public List<ScheduleResponse> generateSchedule() throws ScheduleFailedException {

		List<ScheduleResponse> generatedSchedules = new ArrayList<>();
		/*
		 * Assumption that the schedule will span two weeks and start on the first
		 * working day of the upcoming week i.e. upcoming Monday.
		 */
		List<Date> scheduleDates = CommonUtils.getWorkingDaysForSpanAndDate(SPAN.BIWEEKLY,
				CommonUtils.getUpcomingWorkingDay());

		List<Schedule> currentSchedules = new ArrayList<>();
		while (true) {
			try {
				generatedSchedules = scheduleDates.stream().map(date -> {
					Set<Engineer> invalidEngineers = new HashSet<>();
					Schedule schedule = new Schedule();
					List<Engineer> currentShiftEngineers = new ArrayList<>();
					schedule.setEngineers(currentShiftEngineers);
					schedule.setDate(date);

					logger.info("Creating context for rules processing");
					ScheduleContext context = new ScheduleContext(invalidEngineers, currentShiftEngineers,
							currentSchedules, engineerWeeklyScheduleCountMap);

					logger.info("Creating schedule for shifts in a day");
					IntStream.range(0, SHIFT_TYPE.HALFDAY.getTotalShiftsInDay()).forEach(index -> {
						Engineer selectedEngineer = null;
						while (true) {
							Engineer engineer = dataCache.getRandomEngineer();
							context.setEngineer(engineer);
							if (invalidEngineers.size() == dataCache.getAvailableEngineersCount()) {
								break;
							}

							logger.info("Rules processing started");
							if (!ruleProcessor.validateEngineer(context)) {
								invalidEngineers.add(engineer);
								continue;
							}

							selectedEngineer = engineer;
							break;
						}
						if (null == selectedEngineer) {
							logger.error("Error while creating schedule.");
							throw new ScheduleInvalidException("No engineer found as per rules and current schedule.");
						}
						if (this.engineerWeeklyScheduleCountMap.get(selectedEngineer) != null) {
							this.engineerWeeklyScheduleCountMap.put(selectedEngineer,
									this.engineerWeeklyScheduleCountMap.get(selectedEngineer) + 1);
						} else {
							this.engineerWeeklyScheduleCountMap.put(selectedEngineer, 1);
						}
						logger.info("Added available engineer : " + selectedEngineer.getId());
						currentShiftEngineers.add(selectedEngineer);
					});
					currentSchedules.add(schedule);
					return schedule;
				}).map(schedule -> DtoUtils.prepareScheduleResponse(schedule)).collect(Collectors.toList());
				break;
			} catch (Exception e) {
				logger.error(e.getMessage() + " Error while generating schedule. Trying again.");
				engineerWeeklyScheduleCountMap.clear();
				logger.error("Saved schedules cache cleared.");
				continue;
			}
		}
		return generatedSchedules;
	}
}
