package com.support.wheel.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.time.DateUtils;

import com.support.wheel.constant.SPAN;

/**
 * @author Amit Malik
 *
 */
public class CommonUtils {

	public static int getRandomNumber(int bound) {
		Random random = new Random();
		return random.nextInt(bound);
	}

	/*
	 * Assumption that the schedule will span two weeks and start on the first
	 * working day of the upcoming week.
	 * 
	 * So taking upcoming Monday as next working day.
	 * 
	 */
	public static Date getUpcomingWorkingDay() {
		Calendar calendarDate = Calendar.getInstance();
		int dayOfWeek = calendarDate.get(Calendar.DAY_OF_WEEK);
		calendarDate.add(Calendar.DATE, (9 - dayOfWeek) % 7);
		return calendarDate.getTime();
	}

	/**
	 * @param span
	 * @param startDate
	 * @return
	 */
	public static List<Date> getWorkingDaysForSpanAndDate(SPAN span, Date startDate) {
		List<Date> workDates = new ArrayList<>();
		workDates.add(startDate);
		int counter = 1;
		while (counter < span.getDaysCount()) {
			Date date = DateUtils.addDays(startDate, counter++);
			Calendar instance = Calendar.getInstance();
			instance.setTime(date);
			//Ignoring weekend Sunday and Saturday
			if (Calendar.SUNDAY == instance.get(Calendar.DAY_OF_WEEK)
					|| Calendar.SATURDAY == instance.get(Calendar.DAY_OF_WEEK)) {
				continue;
			}
			workDates.add(date);
		}
		return workDates;
	}
}
