package com.support.wheel.util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.support.wheel.dao.Engineer;
import com.support.wheel.dao.Schedule;
import com.support.wheel.response.model.ScheduleResponse;

/**
 * @author Amit Malik
 *
 */
public class DtoUtils {
	private final static Logger logger = LoggerFactory.getLogger(DtoUtils.class);

	public static ScheduleResponse prepareScheduleResponse(Schedule schedule) {
		List<Engineer> engineers = schedule.getEngineers();
		try {
			return new ScheduleResponse(schedule.getDate(), engineers.get(0).getName(), engineers.get(1).getName());
		} catch (Exception e) {
			logger.error("FAiled " + e.getMessage());
			throw e;
		}
	}

}
