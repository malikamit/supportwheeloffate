package com.support.wheel.WheelofFate;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.support.wheel.exception.ScheduleFailedException;
import com.support.wheel.response.model.ScheduleResponse;
import com.support.wheel.schedule.service.ScheduleService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WheelofFateApplicationTests {

	@Autowired
	private ScheduleService scheduleService;

	@Test
	public void generateScheduleTest() throws ScheduleFailedException {

		List<ScheduleResponse> schedules = scheduleService.generateSchedule();

		Assert.assertThat(schedules, Matchers.notNullValue());
		Assert.assertThat(schedules.size(), Matchers.equalTo(10));

		Set<String> engineerNames = schedules.stream().map(schedule -> schedule.getFirstHalfEmployeeName())
				.collect(Collectors.toSet());
		Assert.assertThat(engineerNames, Matchers.notNullValue());

		engineerNames = schedules.stream().map(schedule -> schedule.getSecondHalfEmployeeName())
				.collect(Collectors.toSet());
		Assert.assertThat(engineerNames, Matchers.notNullValue());

		IntStream.range(0, schedules.size() - 1).forEach(index -> {
			ScheduleResponse day1Schedule = schedules.get(index);
			ScheduleResponse day2Schedule = schedules.get(index + 1);

			// Rule Tested : An engineer can do at most one half day shift in a day.
			Assert.assertThat(day1Schedule.getFirstHalfEmployeeName(),
					Matchers.not(Matchers.equalTo(day1Schedule.getSecondHalfEmployeeName())));
			if (index == schedules.size() - 1) {
				Assert.assertThat(day2Schedule.getFirstHalfEmployeeName(),
						Matchers.not(Matchers.equalTo(day2Schedule.getSecondHalfEmployeeName())));
			}

			Set<String> engineerNamesForTwoConsecutiveDays = new HashSet<>();

			engineerNamesForTwoConsecutiveDays.add(day1Schedule.getFirstHalfEmployeeName());
			engineerNamesForTwoConsecutiveDays.add(day1Schedule.getSecondHalfEmployeeName());
			engineerNamesForTwoConsecutiveDays.add(day2Schedule.getFirstHalfEmployeeName());
			engineerNamesForTwoConsecutiveDays.add(day2Schedule.getSecondHalfEmployeeName());

			// Rule Tested : An engineer cannot have half day shifts on consecutive days.
			Assert.assertThat(engineerNamesForTwoConsecutiveDays.size(), Matchers.equalTo(4));
		});

		Map<String, Integer> engineerWorkDayMap = new HashMap<>();
		schedules.stream().forEach(schedule -> {
			if (engineerWorkDayMap.containsKey(schedule.getFirstHalfEmployeeName())) {
				int count = engineerWorkDayMap.get(schedule.getFirstHalfEmployeeName());
				engineerWorkDayMap.put(schedule.getFirstHalfEmployeeName(), count + 1);
			} else {
				engineerWorkDayMap.put(schedule.getFirstHalfEmployeeName(), 1);
			}

			if (engineerWorkDayMap.containsKey(schedule.getSecondHalfEmployeeName())) {
				int count = engineerWorkDayMap.get(schedule.getSecondHalfEmployeeName());
				engineerWorkDayMap.put(schedule.getSecondHalfEmployeeName(), count + 1);
			} else {
				engineerWorkDayMap.put(schedule.getSecondHalfEmployeeName(), 1);
			}

		});

		// Rule Tested : Each engineer should have completed one whole day of support in
		// any 2 week period.

		Assert.assertThat(engineerWorkDayMap.keySet().size(), Matchers.equalTo(10));
		Assert.assertTrue(engineerWorkDayMap.values().stream().filter(count -> count == 2).count() == 10);
	}
}
