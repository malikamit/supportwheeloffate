package com.support.wheel.util;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import com.support.wheel.constant.SPAN;

public class CommonUtilsTest {

	@Test
	public void testUpcomingMondayUtil() {
		Date responseDate = CommonUtils.getUpcomingWorkingDay();

		Calendar instance = Calendar.getInstance();
		instance.setTime(responseDate);

		Assert.assertThat(instance.get(Calendar.DAY_OF_WEEK), Matchers.equalTo(Calendar.MONDAY));
	}

	@Test
	public void testGetWorkingDaysForSpanAndDate() {
		List<Date> workingDates = CommonUtils.getWorkingDaysForSpanAndDate(SPAN.BIWEEKLY,
				CommonUtils.getUpcomingWorkingDay());

		Assert.assertThat(workingDates, Matchers.notNullValue());
		Assert.assertThat(workingDates.size(), Matchers.equalTo(10));

	}
}
